angular.module('easyfit')

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html"
    })

    .state('app.search', {
      url: "/search",
      views: {
        'menuContent' :{
          templateUrl: "templates/search.html"
        }
      }
    })

    .state('app.login', {
      url: "/login",
      views: {
        'menuContent' :{
          templateUrl: "templates/login.html",
          controller: 'authController'
        }
      }
    })
    .state('app.register', {
      url: "/register",
      views: {
        'menuContent' :{
          templateUrl: "templates/register.html",
          controller: 'authController'
        }
      }
    })

    .state('app.home', {
      url: "/home",
      views: {
        'menuContent' :{
          templateUrl: "templates/home.html",
          controller: 'AppCtrl'
        }
      }
    })

    .state('app.addworkout', {
      url: "/addworkout",
      views: {
        'menuContent' :{
          templateUrl: "templates/add_workout.html"
        }
      }
    })

    .state('app.workouts', {
      url: "/workouts",
      views: {
        'menuContent' :{
          templateUrl: "templates/workouts.html",
          controller: 'AppCtrl'
        }
      }
    })

    .state('app.workout_single', {
      url: "/workout/:workoutId",
      views: {
        'menuContent' :{
          templateUrl: "templates/workout_info.html"
          // controller:'workoutController'
        }
      }
    })

    .state('app.training_single', {
      url: "/training/:trainingId",
      views: {
        'menuContent' :{
          templateUrl: "templates/training_info.html",
          controller: 'AppCtrl'
        }
      }
    })
    .state('app.training_process', {
      url: "/training_process",
      views: {
        'menuContent' :{
          templateUrl: "templates/training_process.html"
        }
      }
    })
    

    .state('app.statistics', {
      url: "/statistics",
      views: {
        'menuContent' :{
          templateUrl: "templates/statistics.html"
        }
      }
    })

    .state('app.results', {
      url: "/results",
      views: {
        'menuContent' :{
          templateUrl: "templates/my_results.html"
        }
      }
    })

    .state('app.trainings_favorite', {
      url: "/trainings_favorite",
      views: {
        'menuContent' :{
          templateUrl: "templates/favorite_trainings.html"
        }
      }
    })

    .state('app.settings', {
      url: "/settings",
      views: {
        'menuContent' :{
          templateUrl: "templates/settings.html"
        }
      }
    })

    .state('app.account', {
      url: "/account",
      views: {
        'menuContent' :{
          templateUrl: "templates/account.html"
        }
      }
    })
//-----------------------------------------
    .state('app.browse', {
      url: "/browse",
      views: {
        'menuContent' :{
          templateUrl: "templates/browse.html"
        }
      }
    })

    .state('app.single', {
      url: "/playlists/:playlistId",
      views: {
        'menuContent' :{
          templateUrl: "templates/playlist.html",
          controller: 'PlaylistCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});