angular.module('easyfit')
.controller('AppCtrl', function($scope,apiservice, sessionInjector, $ionicModal, $user, $timeout, $ionicPopup, $localstorage, $workouts, $rootScope, $http, $state, $config) {
        $scope.training = $workouts.getTraining();
        $scope.rootScope = $rootScope;
        $scope.app_online = $rootScope.online;
        $scope.registerData = {};
        $scope.loginData = {};
        $user.getUserData(function(data){
            $scope.userData = data;
            $workouts.getWorkouts(function(data) {
                $scope.workout = data.workouts;
            });

        })
        $scope.$on('event:auth-loginRequired', function() {
            apiservice.refreshToken();
        });

        

        $scope.showAlert = function(text) {
            var alertPopup = $ionicPopup.alert({
                title: text,
            });
        }
        $scope.goToState = function(state) {
            $state.go(state);
        };
    })
.controller('authController', function($scope,apiservice, $ionicModal, $user, $timeout, $ionicPopup, $localstorage, $workouts, $rootScope, $http, $state, $config) {
        $scope.rootScope = $rootScope;
        $scope.app_online = $rootScope.online;
        $scope.registerData = {};
        $scope.loginData = {};
        // $scope.$on('event:auth-loginRequired', function() {
        //     apiservice.refreshToken();
        // });

        $scope.showAlert = function(text) {
            var alertPopup = $ionicPopup.alert({
                title: text,
            });
        }
        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.rootScope.$watch('online', function(newValue, oldValue) {
            console.log(newValue, oldValue);
        });
        // Triggered in the login modal to close it
        $scope.closeLogin = function() {
            $scope.modal.hide();
        };
        // Open the login modal
        $scope.login = function() {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function() {
            $http({
                url: $config.uri + ":" + $config.port + '/auth/token',
                method: "POST",
                data: {
                    grant_type:'password',
                    client_id:'mobileV1',
                    client_secret:'abc123456',
                    username: $scope.loginData.email,
                    password: $scope.loginData.password,
                }
            }).success(function(data, status) {
                if (status == "200") {
                    $localstorage.setObject('tokens', data)
                    $scope.goToState('app.home');
                }
            })
            .error(function(data, status) {
                console.log(data, status);
            });
        };
        $scope.doRegister = function() {
            console.log($scope.registerData);
            $http({
                url: $config.uri + ":" + $config.port + '/auth/register',
                method: "POST",
                data: {
                    email: $scope.registerData.email,
                    password: $scope.registerData.password,
                    username: {
                        last_name: $scope.registerData.last_name,
                        first_name: $scope.registerData.first_name
                    }
                }
            }).success(function(data, status) {
                if (status == "200" && data.status == "OK") {
                    $scope.goToState('app.login');
                }
            });
        };
        $scope.goToState = function(state) {
            $state.go(state);
        };
    });