var module = angular.module('easyfit');

module.factory('sessionInjector', ['$localstorage', function($localstorage) {
    var sessionInjector = {
        request: function(config) {
            var tokens = $localstorage.getObject('tokens');
            var access_token = tokens.access_token;
            if(!access_token){
                console.log('no token');
                return config;
            } else {
                config.headers['Authorization'] = 'Bearer ' + access_token;
                return config;
        }
    }
};
    return sessionInjector;
}]);
 
module.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('sessionInjector');
}]);