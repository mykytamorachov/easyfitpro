angular.module('easyfit')
.factory('apiservice', ['$http','$config','authService', '$localstorage', function($http, $config,authService, $localstorage){
	var service = {
		apiRequest: function (params) {
                return $http(params);
        },
        refreshToken: function(){
            var tokens = $localstorage.getObject('tokens');
            $http({
                url: $config.uri + ":" + $config.port + '/auth/token',
                method: "POST",
                data: {
                    grant_type:'refresh_token',
                    client_id:'mobileV1',
                    client_secret:'abc123456',
                    refresh_token: tokens.refresh_token
                }
            }).success(function(data, status) {
                if (status == "200") {
                    $localstorage.setObject('tokens', data)
                    authService.loginConfirmed();
                }
            });
        }
	};
	return service;
}]);