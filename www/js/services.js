angular.module('ionic.utils', [])

.factory('$localstorage', ['$window',
    function($window) {
        return {
            set: function(key, value) {
                $window.localStorage[key] = value;
            },
            get: function(key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function(key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function(key) {
                return JSON.parse($window.localStorage[key] || '{}');
            }
        }
    }
]);

angular.module('easyfit')

.factory('$user', ['$http','$q', 'apiservice', '$config' , 
    function($http, $q, apiservice, $config){
        return { 
            getUserData: function(cb){
                var url = $config.uri + ":" + $config.port + '/api/userinfo';
                var resPromise = apiservice.apiRequest({
                    url: url,
                    method: "GET"
                });
                if ( resPromise != false ) {
                    resPromise
                        .success(function(data, status, headers, config) {
                            cb(data);
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                        });
                } else {
                    cb({});
                }
            }
        }
}])

.factory('$workouts', ['$q','$http', '$rootScope', '$localstorage', 'apiservice',
        function($q,$http, $rootScope, $localstorage, apiservice) {
            return {
                getWorkouts: function(cb) {
                    var url = 'http://localhost:1337/workouts';
                    var resPromise = apiservice.apiRequest({
                        url: url,
                        method: "GET"
                    });
                    if ( resPromise != false ) {
                        resPromise
                        .success(function(data, status, headers, config) {
                            cb(data);
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                        });
                    } else {
                        cb({});
                    }
                    // return results;
                },

                getWorkoutById: function(workout_id, url, cb)
                {
                    $http.get(url+workout_id).
                    success(function(data, status, headers, config) {
                        cb(data);
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                    });
                },
                getWorkoutList: function($http) {
                    var list;
                    if($localstorage.getObject('workout_list').length){
                        list = $localstorage.getObject('workout_list')
                    }
                    else{
                    list = [{
                        workout_name: "Workout 1",
                        workout_id: 8,
                        workout_trainings: [{
                            training_name: "Training1",
                            training_id: 3,
                            excersizes: [{
                                name: "ololo",
                                sets: 5,
                                repeats: 10,
                                set_type: "Default"
                            }, {
                                name: "ololo",
                                sets: 5,
                                repeats: 10,
                                set_type: "Default"
                            }, {
                                name: "ololo",
                                sets: 5,
                                repeats: 10,
                                set_type: "Default"
                            }]
                        }]
                    }, {
                        workout_name: "Workout2",
                        workout_id: 2,
                        workout_trainings: [{
                            training_name: "Training2",
                            training_id: 4,
                            excersizes: [{
                                name: "trololo",
                                sets: 5,
                                repeats: 10,
                                set_type: "Default"
                            }, {
                                name: "trololo",
                                sets: 5,
                                repeats: 10,
                                set_type: "Default"
                            }, {
                                name: "trololo",
                                sets: 5,
                                repeats: 10,
                                set_type: "Default"
                            }]
                        }]
                    }];
                    $localstorage.setObject('workout_list',list)
                    }
                    return list;
                },
                getTraining: function($http, $localStorage) {
                    var training = {
                        training_name: "Тенировка рук",
                        training_author: "Вася",
                        training_id: 3,
                        training_type: "Зал",
                        excersizes: [{
                                name: "Подъемы стоя 1",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }, {
                                name: "Подъемы сидя",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }, {
                                name: "Подъемы сидя",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }, {
                                name: "Подъемы сидя",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }, {
                                name: "Подъемы сидя",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }, {
                                name: "Подъемы сидя",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }, {
                                name: "Подъемы сидя",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }, {
                                name: "Молоток",
                                sets: 5,
                                repeats: 10,
                                set_type: "Обычный",
                                pause:60
                            }]
                        };
                        return training;
                    }
                }
            }
            ])

    .factory('results', ['$http', '$rootScope',
        function() {
            return {
                getResults: function(cb) {
                    var results = {
                        weight: 88,
                        chest_press: 90,
                        sit: 150,
                        deadlift: 150
                    }
                    $http.get('/someUrl', {
                        data: results
                    }).
                    success(function(data, status, headers, config) {
                        cb(data);
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                    });
                    return results;
                },

                setResults: function($http, results) {
                    $http.post('/someUrl', {
                        data: results
                    }).
                    success(function(data, status, headers, config) {
                        // this callback will be called asynchronously
                        // when the response is available
                    }).
                    error(function(data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
                }
            };
        }
    ])